// FirstDemo_SimpleConnect_Cilent.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "lib_acl.hpp"

using namespace acl;

int _tmain(int argc, _TCHAR* argv[])
{
	log::stdout_open(true);

	acl_cpp_init();
	const char* server_addr = "192.168.1.153:8081";
	int conn_timeout = 10;
	int io_timeout = 10;
	socket_stream conn;
	if (!conn.open(server_addr, conn_timeout, io_timeout))
	{
		printf("conn error:%s", last_error());
		return -1;
	}

	const char* reqContent = "helloworld!\r\n";
	string buf;

	//向服务器发送消息
	if (conn.write(reqContent, strlen(reqContent)) == -1)
	{
		printf("req error:%s", last_error());
		return -1;
	}

	//从服务器端获取消息
	if (!conn.read(buf, false))
	{
		printf("read one line from server error: %s\r\n", last_serror());
		return -1;
	}

	printf("response:%s", buf.c_str());
	return 0;
}

