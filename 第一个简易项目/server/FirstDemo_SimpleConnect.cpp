// FirstDemo_SimpleConnect.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "lib_acl.hpp"

using namespace acl;

class client_thread : public thread
{
public:
	client_thread(socket_stream *client) :client_(client)
	{

	}

	~client_thread()
	{
		delete client_;
	}
protected:

	void *run()
	{
		string buf;
		while (true)
		{
			if (client_->gets(buf, true) == false)
				return NULL;
			break;
		}
		printf("data is: %s", buf.c_str());

		if (client_->write(buf,true) == -1) return NULL;
	}
private:
	socket_stream *client_;
};

int _tmain(int argc, _TCHAR* argv[])
{
	log::stdout_open(true);

	acl_cpp_init();
	server_socket serverSocket;
	if(!serverSocket.open("192.168.1.153:8081"))
	{
		printf("错误码为:%d", last_error());
		return -1;
	}
	while (true)
	{
		socket_stream *client = serverSocket.accept();
		if (client == NULL)
		{
			printf("accept error:%d", last_error());
			return -1;
		}

		client_thread *thread = new client_thread(client);
		// 将线程设为分离模式，这样当线程退出时会自行释放线程相关资源
		thread->set_detachable(true);
		thread->start();
	}
	return 0;
}
